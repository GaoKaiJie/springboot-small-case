package com.example.mapper;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.model.Book;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class BookMapperTest {

    @Autowired
    BookMapper bookMapper;

    @Test
    void testselect(){
        System.out.println(bookMapper.selectList(null));
    }

    @Test
    void testSelectById(){
        System.out.println(bookMapper.selectById(4));
    }

    @Test
    void testSave(){
        Book book = new Book();
        book.setType("测试数据123");
        book.setName("测试数据123");
        book.setDescription("测试数据123");
        bookMapper.insert(book);
    }

    @Test
    void testUpdate(){
        Book book = new Book();
        book.setId(30);
        book.setType("测试数据222");
        book.setName("测试数据222");
        book.setDescription("测试数据222");
        bookMapper.updateById(book);
    }

    @Test
    void testDelete(){
        int i = bookMapper.deleteById(30);
        System.out.println(i);
    }

    @Test
    void testGetAll(){
        bookMapper.selectList(null);
    }

    @Test
    void testGetPage(){
        IPage page = new Page(1,5);
        bookMapper.selectPage(page,null);
        System.out.println(page.getRecords());
    }

    @Test
    void testGetBy(){
        String name = "5";
        IPage page = new Page(1,10);
        LambdaQueryWrapper<Book> qw = new LambdaQueryWrapper<>();
        qw.like(name != null,Book::getName,name);
        bookMapper.selectList(qw);

    }
}
