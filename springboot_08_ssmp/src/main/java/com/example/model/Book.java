package com.example.model;

import lombok.Data;

// 使用lombok简化实体类开发

@Data
public class Book {
    private Integer id;
    private String type;
    private String name;
    private String description;


}
